" VIM syntax file
" Language:     goal
" Maintainer:   Yon <anaseto@bardinflor.perso.aquilenet.fr>
" Last Change:  2024 Oct 31
"
" originally adapted from https://codeberg.org/ngn/k/vim-k/syntax.vim
if version < 600
	syntax clear
elseif exists("b:current_syntax")
	finish
endif

let s:cpo_sav = &cpo
set cpo&vim

setl isk=a-z,A-Z,48-57,.
sy iskeyword a-z,A-Z,48-57
sy case match
sy sync minlines=500

" goal_e highlights errors, which by default everything can be.
sy match goal_e   /\i\+\|\S/
sy match goal_s   /[\\'`]/
sy match goal_w1  /[\\\/'`´¨]:\?\([)}\];\[\n]\)\@=/
sy match goal_u   /[+\-*%!&|<>=~,^#_$?@.«»¿:]:\?/                 nextgroup=goal_w
sy match goal_kv  /:\([\\\/'`´¨]\)\@!/
sy match goal_s   /:\([\[\\\/'´`¨:;[)}\]\[\n]\)\@!/
sy match goal_v   /[+\-*%!&|<>=~,^#_$?@:.«»¿]/                    nextgroup=goal_w     contained
sy match goal_a   /[+\-*%!&|<>=~,^#_$?@.«»¿]:\([)}\];\[ \n]\)\@!/ nextgroup=@goal_goal contained
sy match goal_a   /[+\-*%!&|<>=~,^#_$?@.«»¿]:\(\s\+[\\'`]\)\@=/   nextgroup=@goal_goal contained
sy match goal_a   /:\([)}\];\[ \t\n]\)\@!/                        nextgroup=@goal_goal contained
sy match goal_a   /:\(\s\+[\\'`]\)\@=/                            nextgroup=@goal_goal contained
sy match goal_a   /::/                                            nextgroup=@goal_goal contained
sy match goal_dd  /\.\./                                          nextgroup=@goal_goal
sy match goal_uc  /::\([)}\];\[ \t\n]\)\@=/                       nextgroup=goal_w
sy match goal_w   /[\\\/'`´¨]:\?/                                 nextgroup=goal_w     contained
sy match goal_i   /\v\a[[:alnum:]]*/                              nextgroup=@goal_vw
sy match goal_xyz /\<[xyzo]\>/                                    nextgroup=@goal_vw
sy match goal_i   /\v\a[[:alnum:]]*(\.\a[[:alnum:]]*)+/                   nextgroup=@goal_vw
sy match goal_i   /\v\a[[:alnum:]]*(\.\a[[:alnum:]]*)*\.\.\a[[:alnum:]]*/ nextgroup=@goal_vw
sy match goal_ix  /\<[xyzo]\>/ contained display
sy match goal_i   /\v[xyzo]\.\.\a[[:alnum:]]*/ nextgroup=@goal_vw contains=goal_ix
sy match goal_ku  /\<rx\>/ nextgroup=@goal_w
sy keyword goal_ku nextgroup=goal_w
	\ abs uc error eval firsts json ocount panic sign
	\ abspath chdir close dirfs flush mkdir remove
	\ cos round sin sqrt
sy match goal_ku /\v<(rt\.log|rt\.seed|rt\.time|rt\.get|rt\.try)>/ nextgroup=goal_w
sy keyword goal_kv nextgroup=goal_w
	\ and or
	\ in csv nan rotate rshift shift sub utf8
	\ env glob import open print read rename run say shell stat subfs
	\ atan exp log time

sy match goal_ndi nextgroup=@goal_vw "-\=\(0\|[1-9]_\?\(\d\|\d\+_\?\d\+\)*\)\%([Ee][-+]\=\d\+\)\=\>"
sy match goal_nxi nextgroup=@goal_vw "-\=0[xX]_\?\(\x\+_\?\)\+\>"
sy match goal_noi nextgroup=@goal_vw "-\=0[oO]\?_\?\(\o\+_\?\)\+\>"
sy match goal_nbi nextgroup=@goal_vw "-\=0[bB]_\?\([01]\+_\?\)\+\>"
sy match goal_nf nextgroup=@goal_vw "-\=\d\+\.\d*\%([Ee][-+]\=\d\+\)\=\>"
sy match goal_nd nextgroup=@goal_vw "-\=\(\d\+\%(\.\d*\)\=\%(ns\|us\|ms\|s\|m\|h\)\)\+\>"
sy match goal_ns nextgroup=@goal_vw "-\=0w\>"
sy match goal_ns nextgroup=@goal_vw "0n\>"
sy match goal_ns nextgroup=@goal_vw "0i\>"

sy cluster goal_n contains=goal_ndi,goal_nxi,goal_noi,goal_nbi,goal_nf,goal_nd,goal_ns

sy region goal_rq matchgroup=goal_rq start=/rq\z([:+\-*%!&|=~,^#_?@`/]\)/ end=+\z1+ skip=/\z1\z1/ nextgroup=@goal_vw
sy region goal_rx matchgroup=goal_rx start=/rx\z([:+\-*%!&|=~,^#_?@`/]\)/ end=+\z1+ skip=/\\\z1/ nextgroup=@goal_vw contains=@goal_rxc
sy region goal_qq matchgroup=goal_qq start=/qq\z([:+\-*%!&|=~,^#_?@`/]\)/ end=+\z1+ skip=/\\\z1/ nextgroup=@goal_vw contains=@goal_qqc
sy region goal_qq matchgroup=goal_qq start=/"/ end=/"/ skip=/\\"/   nextgroup=@goal_vw contains=@goal_qqc

sy match goal_rxs display contained /[.+*?()|\[\]{}^$]/
sy match goal_rxe display contained /\v\[\[:\a+:\]\]/
sy match goal_rxe display contained /\\./
sy match goal_rxs display contained /\\[AbBzQE]/
sy cluster goal_rxc contains=goal_qo,goal_qx,goal_rxe,goal_rxs

sy match goal_qi /\v\$\a[[:alnum:]]*(\.\a[[:alnum:]]*)?(\.\.\a[[:alnum:]]*)?|\$\{\a[[:alnum:]]*(\.\a[[:alnum:]]*)?(\.\.\a[[:alnum:]]*)?\}/ contained
sy match goal_qo display contained "\\[0-7]\{3}"
sy match goal_qc display contained +\\[abfnrtv\\'"]+
sy match goal_qx display contained "\\x\x\{2}"
sy match goal_qu display contained "\\u\x\{4}"
sy match goal_qU display contained "\\U\x\{8}"
sy match goal_qe display contained +\\[^0-7xuUabfnrtv\\'"]+
sy cluster goal_qqc contains=goal_qo,goal_qc,goal_qx,goal_qu,goal_qU,goal_qe,goal_qi

sy region goal_pr matchgroup=goal_p start=/(/  end=/)/ nextgroup=@goal_vw contains=@goal_goal,goal_ps,goal_pe
sy region goal_br matchgroup=goal_b start=/\[/ end=/]/ nextgroup=@goal_vw contains=@goal_goal,goal_bs,goal_be
sy region goal_lr matchgroup=goal_l start=/{\(\[[^\]]*\]\)\=/ end=/}/ nextgroup=@goal_vw contains=@goal_goal,goal_ls,goal_le
sy match goal_pe /[]}]/ contained
sy match goal_be /[})]/ contained
sy match goal_le /[)]]/ contained
sy match goal_ps /;/    contained
sy match goal_bs /;/    contained
sy match goal_ls /;/

sy region goal_c matchgroup=goal_c start=/\([{(\[;]\)\@<=\/\(:\)\@!/ end=/$/ contains=goal_cc
sy region goal_c matchgroup=goal_c start=/\(^\/\| \/\|\%^#!\)/ end=/$/ contains=goal_cc
sy region goal_c matchgroup=goal_c start=/^\/\s*$/                end=/^\\\s*$/ contains=goal_cc

sy keyword goal_cc contained TODO FIXME XXX BUG NOTE

" goal_vw: dyadic verbs and adverbs
sy cluster goal_vw contains=goal_v,goal_w,goal_a,goal_uc,goal_dd
" goal_goal: everything else
sy cluster goal_goal  contains=goal_e,goal_u,goal_uc,goal_s,goal_ku,goal_kv,goal_un,goal_i,goal_xyz,goal_c,@goal_n,
	\ goal_bq,goal_rq,goal_qq,goal_rx,goal_pr,goal_br,goal_lr,goal_w1,goal_dd

hi link goal_e   error
hi link goal_u   function
hi link goal_v   type
hi link goal_w   operator
hi link goal_w1  operator
hi link goal_i   variable
hi link goal_ku  goal_u
hi link goal_uc  goal_u
hi link goal_kv  goal_v
hi link goal_xyz special
hi link goal_ix  goal_xyz
hi link goal_s   special
hi link goal_a   statement
hi link goal_dd  statement
hi link goal_bq  string
hi link goal_rq  string
hi link goal_qq  string
hi link goal_qqi preproc
hi link goal_qqc preproc
hi link goal_qi  goal_qqc
hi link goal_qo  goal_qqc
hi link goal_qc  goal_qqc
hi link goal_qx  goal_qqc
hi link goal_qu  goal_qqc
hi link goal_qU  goal_qqc
hi link goal_qe  error
hi link goal_rx  string
hi link goal_rxe preproc
hi link goal_rxs special
hi link goal_ndi number
hi link goal_nxi number
hi link goal_noi number
hi link goal_nbi number
hi link goal_nf  number
hi link goal_nd  number
hi link goal_ns  number
hi link goal_p   nontext
hi link goal_b   goal_p
hi link goal_l   special
hi link goal_pe  error
hi link goal_be  error
hi link goal_le  error
hi link goal_ps  goal_p
hi link goal_bs  goal_b
hi link goal_ls  goal_l
hi link goal_c   comment
hi link goal_cc  TODO

let b:current_syntax='goal'

let &cpo = s:cpo_sav
unlet! s:cpo_sav
